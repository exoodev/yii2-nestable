<?php

namespace exoo\nestable;

/**
 * Nestable Asset bundle.
 */
class NestableAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/nestable2/dist';
    /**
     * @inheritdoc
     */
    public $js = [
        'jquery.nestable.min.js',
    ];
    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
